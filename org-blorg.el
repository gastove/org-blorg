;;; org-blorg.el --- Do You a Weird Blog with Org
;;
;;; Commentary:
;; Why do a usual thing when you could do a super weird one! Welcome to
;; `org-blorg'.
;;
;; `org-blorg' works on this idea: many tools exist to let you publish a
;; directory of markdown documents as posts on a blog. I don't want to do that.
;; I want to write my blog:
;;
;; 1. In `org-mode'
;; 2. With tidy convenience methods for setting meta data and properties
;; 3. Outputting a format I can suck up and use basically anywhere HTML is served.
;;
;; This tool is that.
;;; Code:

(require 'org)
(require 'dash)
(require 's)
(require 'f)

;;--------------------------Public Configuration Vars----------------------------
(defvar-local org-blorg-publish-dir "/tmp/org-blorg"
  "Directory to write exported content to.")

(defvar-local org-blorg-publish-tags "+live -draft"
  "An `org-mode' properties/tags query string; every entry that
  matches will be considered valid for export. See the docstring
  of `org-map-entries' or the org manual on tags/properties match
  for more details.")

;; TODO: do I really want this?
(defvar-local org-blorg-html-meta-property-names-alist
    `(("PUBLICATION_DATE" . "Published")
      ("CATEGORIES"       . "Published in")
      ("AUTHOR"           . "Who wrote this"))
  "An alist mapping org property names to the name that should be
  inserted in the HTML metadata footer on export. This variable
  also controls which properties are included at all.")

(defvar-local org-blorg-meta-tag "POST-META"
  "The metadata footer div appended to exported files will be
  named this.")

(defvar-local org-blorg-max-slug-characters 20
  "If no SLUG property is set on a heading, `org-blorg' will make
  one from the heading of the post; it will be capped to this
  length.")

(defvar-local org-blorg-filtered-meta-props
    `("CATEGORY" "BLOCKED" "FILE" "PRIORITY" "ALLTAGS")
  "Org mode includes a set of properties we don't want to include
  in the metadata footer. Every element of this list will be
  filtered out of the metadata.")

;;----------------------------------Insertion------------------------------------

(defun org-blorg--insert-after-props (s)
  "Insert S immediately after the PROPERTIES drawer of an entry."
  (search-forward-regexp org-property-end-re)
  (forward-char 1)
  (insert s)
  (newline))

(defun org-blorg--insert-at-end-of-entry (s)
  "Insert S at the end of an entry; does not preserve point."
  (save-restriction
    (org-narrow-to-subtree)
    (goto-char (point-max))
    (newline)
    (insert s)))

;;------------------------------- Link Handling -------------------------------;;
;; This allows links to be resolved to include a static asset root.

;; <video width="500" height="200" controls loop video controls autoplay>
;;     <source src="http://example.com/movie.mp4" type="video/mp4">
;;     Your browser does not support the video tag.
;; </video>

(defvar org-blorg-static-content-root "https://storage.googleapis.com/static.gastove.com")

(defun org-blorg--render-video-html (link &optional height width controls)
  (-let* ((h (if height (format "height=\"%s\"" height)))
          (w (if width (format "width=\"%s\"" width)))
          (c (if controls (format "controls %s" controls) "controls loop video controls autoplay muted"))
          (l (cond
              ((not org-blorg-static-content-root) link)
              ((or (s-ends-with? "/" org-blorg-static-content-root)
                   (s-starts-with? "/" link))
               (s-concat org-blorg-static-content-root link))
              (:else (s-join "/" (list org-blorg-static-content-root link)))))
          (props (concat h w c)))
    (format "<video %s>\n<source src=\"%s\" type=\"video/mp4\">\nYour browser does not support the video tag\n</video>" props l)))

(defun org-blorg--export-link (link desc info)
  (-let [type (org-element-property :type link)]
    (cond
     ((string= type "video")
      (-let ((path (org-element-property :path link)))
        (org-blorg--render-video-html path)))
     (:else (org-html-link link desc info)))))

(defun org-blorg--export-video-link (path desc format)
  "BEEP BEEP BLOOP BLOOP")

;; (defun org-blorg--export-rel-url (path desc format)
;;   (cl-case format
;;     (html (format "<a href=\"%s\">%s</a>" path (or desc path)))
;;     (latex (format "\href{%s}{%s}" path (or desc path)))
;;     (otherwise path)))

(org-link-set-parameters "video" :follow #'browse-url :export #'org-blorg--export-video-link)

;;--------------------------------- Exporter ---------------------------------;;
(defun org-blorg--src-block-to-html (src-block _contents info)
  "Transcode a SRC-BLOCK element from Org to HTML. Unlike Org,
write a damn <pre><code> tag pair, instead of the unparseable
flim-flam org does."
  (let* ((lang (org-element-property :language src-block))
         (code (org-html-format-code src-block info))
         (label (let ((lbl (and (org-element-property :name src-block)
                                (org-export-get-reference src-block info))))
                  (if lbl (format " id=\"%s\"" lbl) ""))))
    (if (not lang)
        (format "<pre><code>\n%s\n</code></pre>" code)
      (format "<div class=\"org-src-container\">\n%s\n</div>"
              (format "<pre class=\"line-numbers\"><code class=\"language-%s\">%s</code></pre>" lang code)))))

(defun org-blorg--html-footnote-section (info)
  "Currently a copy-paste of `org-html-footnote-section'. I'll
customize this as I go. I just do not want the definition in its
own div! A div is a block element; I want the <sup> and the
definition together in a <span>, or together in a /single/ div."
  (pcase (org-export-collect-footnote-definitions info)
    (`nil nil)
    (definitions
     (format
      (plist-get info :html-footnotes-section)
      (org-html--translate "Footnotes" info)
      (format
       "\n%s\n"
       (mapconcat
        (lambda (definition)
          (pcase definition
            (`(,n ,_ ,def)
             ;; `org-export-collect-footnote-definitions' can return
             ;; two kinds of footnote definitions: inline and blocks.
             ;; Since this should not make any difference in the HTML
             ;; output, we wrap the inline definitions within
             ;; a "footpara" class paragraph.
             (let ((inline? (not (org-element-map def org-element-all-elements
                                   #'identity nil t)))
                   (anchor (org-html--anchor
                            (format "fn.%d" n)
                            n
                            (format " class=\"footnum\" href=\"#fnr.%d\" role=\"doc-backlink\"" n)
                            info))
                   (contents (org-trim (org-export-data def info))))
               (format "<div class=\"footdef\">%s\n%s</div>"
                       anchor
                       (format "<div class=\"footblock\" role=\"doc-footnote\">%s</div>"
                               contents
                               ))))))
        definitions
        "\n"))))))

(defun org-blorg--html-inner-template (contents info)
  "Wraps `org-html-inner-template' to allow replacing the
`org-html-footnote-section' function with our own implementation.
The footnote definition should *not* be wrapped in a div, c'mon."
  (concat
   ;; Table of contents.
   (let ((depth (plist-get info :with-toc)))
     (when depth (org-html-toc depth info)))
   ;; Document contents.
   contents
   ;; Footnotes section.
   (org-blorg--html-footnote-section info)))

(org-export-define-derived-backend 'org-blorg-html 'html
  :translate-alist '((src-block . org-blorg--src-block-to-html)
                     (link . org-blorg--export-link)
                     (result-block . org-blorg--result-block-to-html)
                     (inner-template . org-blorg--html-inner-template)
                     ))

(defun org-blorg--html-export-as-html
    (&optional async subtreep visible-only body-only ext-plist)
  (interactive)
  (org-export-to-buffer 'org-blorg-html "*Org-Blorg HTML Export*"
    async subtreep visible-only body-only ext-plist
    (lambda () (set-auto-mode t))))

(defun org-blorg--html-export-to-html
    (&optional async subtreep visible-only body-only ext-plist)
  (interactive)
  (let* ((extension (concat "." (or (plist-get ext-plist :html-extension)
                                    org-html-extension
                                    "html")))
         (file (org-export-output-file-name extension subtreep org-blorg-publish-dir))
         (org-export-coding-system org-html-coding-system))
    (org-export-to-file 'org-blorg-html file
      async subtreep visible-only body-only ext-plist)))

;;-----------------------------------Export-------------------------------------

(defun org-blorg--clean-heading (heading)
  "Parses HEADING, returning a list of words, normalized to
  lower-case, with no punctuation."
  ;; This mutates the variable, rather than returning a new one
  (set-text-properties 0 (length heading) nil heading)
  (->> heading
       (s-downcase)
       (s-match-strings-all "\\<[a-z0-9]+\\>")
       (apply #'-concat)))

(defun org-blorg--make-post-slug ()
  (-let* ((heading (org-get-heading t t t t))
          (header-pieces (org-blorg--clean-heading heading))
          (header-word-len (- org-blorg-max-slug-characters 11))
          (slug-date (format-time-string "%Y-%m-%d"))
          (slug (-reduce (lambda (f s)
                           (-let [n (s-join "-" (list f s))]
                             (if (< (length n) org-blorg-max-slug-characters) n f)))
                         header-pieces)))
    (s-concat slug-date "--" slug)))

(defun org-blorg--make-export-file-name ()
  (-let [slug (org-entry-get (point) "SLUG")]
    (s-join "/" (list org-blorg-publish-dir (s-concat slug ".html")))))

(defvar org-blorg--property-defaults-alist
  '(("SLUG"             . org-blorg--make-post-slug)
    ("EXPORT_FILE_NAME" . org-blorg--make-export-file-name)
    ("PUBLICATION_DATE" . (lambda () (format-time-string "%Y-%m-%dT%H:%M:%S")))
    ("CATEGORIES"       . "Bloggery")
    ("TITLE"            . (lambda () (org-get-heading t t t t)))
    ("SUMMARY"          . "Fill me in!")))

(defvar org-blorg--export-time-property-alist
  '(("LAST_UPDATED" . (lambda () (format-time-string "%Y-%m-%dT%H:%M:%S")))))

(defun org-blorg--resolve-export-meta-props (export-prop-name tpl)
  "Load the property value of EXPORT-PROP-NAME from the heading
under point, then retreives the meta-name from
`org-blorg-html-meta-property-names-alist'; returns both
formatted as a string using TPL."
  (-let ((entry-prop-val (org-entry-get (point) export-prop-name t))
         (meta-name      (alist-get export-prop-name org-blorg-html-meta-property-names-alist)))
    (format tpl meta-name entry-prop-val)))

(defun org-blorg--format-categories (categories-prop)
  (-let* ((opener "<CATEGORIES>")
          (closer "</CATEGORIES>")
          (tag-tpl "<CATEGORY>%s</CATEGORY>")
          (cats-list (s-split-words (cdr categories-prop)))
          (tags-list (-map (lambda (c) (format tag-tpl c)) cats-list))
          (tags-str  (s-join "\n" tags-list)))
    (s-join "\n" (list opener tags-str closer))))

(defun org-blorg--compile-tags ()
  (-let* ((props (org-entry-properties))
          (filtered-props (-filter
                           (lambda (prop) (not (-contains? org-blorg-filtered-meta-props (car prop))))
                           props)))

    (-map (lambda (prop)
            (-let [tpl "<%s>%s</%s>"]
              (if (string= (car prop) "CATEGORIES")
                  (org-blorg--format-categories prop)
                (-let ((tag-name (s-upcase (car prop)))
                       (tag-val  (cdr prop)))
                  (format tpl tag-name tag-val tag-name)))))
          filtered-props)))

(defun org-blorg--props-to-xml-meta ()
  "Converts the properties block of the heading under point into
an XML meta block."
  (-let* ((meta-opener (format "<%s>" org-blorg-meta-tag))
          (meta-closer (format "</%s>" org-blorg-meta-tag))
          (tags (s-join "\n" (org-blorg--compile-tags))))

    (s-join "\n" (list meta-opener tags meta-closer))))

(defun org-blorg--resolve-property-value (default-prop)
  "Given DEFAULT-PROP, whose car is a property name, check if that
property is already set at point. If yes, use the existing
property value. If no, the cdr of DEFAULT-PROP can be a value or
a function of no arguments that generates a value."
  (-let* ((heading-prop     (org-entry-get (point) (car default-prop) t))
          (prop-name        (car default-prop))
          (prop-val-or-func (cdr default-prop))
          (prop-val         (or heading-prop
                                (if (functionp prop-val-or-func)
                                    (funcall prop-val-or-func)
                                  prop-val-or-func))))
    `(,prop-name . ,prop-val)))

(defun org-blorg--resolve-and-set-properties-from-alist (an-alist)
  (-map (lambda (prop)
          (-let [resolved-prop (org-blorg--resolve-property-value prop)]
            (org-entry-put (point) (car resolved-prop) (cdr resolved-prop))))
        an-alist))

(defun org-blorg--assure-properties ()
  (org-blorg--resolve-and-set-properties-from-alist org-blorg--property-defaults-alist))

(defun org-blorg--set-export-time-properties ()
  (org-blorg--resolve-and-set-properties-from-alist org-blorg--export-time-property-alist))


(defun org-blorg--do-export-with-meta-and-manifest ()
  (-let ((export-file (org-blorg--html-export-to-html nil t nil t))
         (meta (org-blorg--props-to-xml-meta))
         ;; (inhibit-message t)
         )
    (append-to-file "\n" nil export-file)
    (append-to-file meta nil export-file)
    ;; Parsing the resulting file is more reliable with a final newline
    (append-to-file "\n" nil export-file)))

(defun org-blorg--create-match ()
  "Creates a match string for use in org-map-entries by assuring
+LEVEL=1 is concatenated in."
  (concat "+LEVEL=1" org-blorg-publish-tags))

(defun org-blorg--get-all-buffer-tags-raw ()
  (-flatten
   (save-excursion
     (org-map-entries (lambda () (org-get-tags (point) 't) ) (org-blorg--create-match) 'file))))

(defun org-blorg--tags-manifest ()
  (-let ((tagset (make-hash-table :test 'equal))
         (raw-tags (org-blorg--get-all-buffer-tags-raw)))
    (-map #'(lambda (tag) (puthash tag 't tagset)) raw-tags)
    (-let [keys (hash-table-keys tagset)]
      (with-temp-file (f-join org-blorg-publish-dir "tags-manifest.json")
        (insert (json-serialize (list :tags (vconcat (-distinct keys))))))
      )
    ))

(defun org-blorg--assure-base-properties ()
  (save-excursion
    (org-map-entries
     #'org-blorg--assure-properties (org-blorg--create-match) 'file)))

(defun org-blorg--set-on-export-properties ()
  (save-excursion
    (org-map-entries
     #'org-blorg--set-export-time-properties (org-blorg--create-match) 'file)))


;;--------------------------- Interactive Commands ---------------------------;;

(defun org-blorg-set-base-properties ()
  (interactive)
  (save-excursion
    (org-blorg--assure-properties)))

(defun org-blorg-last-updated-on ()
  (interactive)
  (-let ((last-updated-prop "LAST_UPDATED")
         (timestamp (format-time-string "%Y-%m-%dT%H:%M:%S")))
    (org-set-property last-updated-prop timestamp)))

(defun org-blorg-publish ()
  "Publishes the current file by assuring export properties, then
exporting every heading to its own file."
  (interactive)
  (org-blorg-set-base-properties)
  (org-blorg--set-on-export-properties)
  (unless (f-exists? org-blorg-publish-dir)
    ;; How... do I make this work
    (if (yes-or-no-p (format "Directory %s does not exist; create it?" org-blorg-publish-dir))
        (make-directory org-blorg-publish-dir t)
      (error "Dir %s does not exist; create it, and try again!"
             org-blorg-publish-dir)))

  (org-blorg--tags-manifest)
  (save-excursion
    (org-map-entries #'org-blorg--do-export-with-meta-and-manifest (org-blorg--create-match) 'file))
  (message "Publishing complete"))

;;-------------------------------Mode Definition---------------------------------

(define-derived-mode org-blorg-mode org-mode
  "Blorg!"
  "An org-mode based mode for blogging.")

(provide 'org-blorg)
;;; org-blorg.el ends here
